//
//  ViewController.m
//  TestTask
//
//  Created by Hrybenuik Mykola on 9/25/15.
//  Copyright © 2015 Hrybenuik Mykola. All rights reserved.
//

#import "ViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "FileSystem.h"

@interface ViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ViewController {
    FileSystem *_fileSystem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL  URLWithString:@"http://hzapp.info/emali/"]]];
    _webView.delegate = self;
    _fileSystem = [FileSystem new];
    __weak typeof(self) weakSelf = self;
    [_fileSystem setCallbackScript:^(NSString *script) {
        [weakSelf.webView stringByEvaluatingJavaScriptFromString:script];
    }];
    
}

#pragma mark - WebViewDelegate's Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    return YES;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    JSContext *context = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    
    [context setExceptionHandler:^(JSContext *context, JSValue *value) {
        NSLog(@"WEB JS: %@", value);
    }];
    
    context[@"FileSystem"] = _fileSystem;
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}

@end
