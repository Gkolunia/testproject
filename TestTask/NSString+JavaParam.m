//
//  NSString+JavaParam.m
//  TestTask
//
//  Created by Hrybenuik Mykola on 9/25/15.
//  Copyright © 2015 Hrybenuik Mykola. All rights reserved.
//

#import "NSString+JavaParam.h"

@implementation NSString (JavaParam)

- (NSString *)javaFormatParam {
    return [NSString stringWithFormat:@"'%@'", self];
}

@end
