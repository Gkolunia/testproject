//
//  TSFileManager.h
//  TestTask
//
//  Created by Hrybenuik Mykola on 9/25/15.
//  Copyright © 2015 Hrybenuik Mykola. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^TSFileManagerCallbackBlock)(NSArray *params, NSString *status);

@interface TSFileManager : NSObject

- (void)saveFileWithName:(NSString*)name andContent:(NSString*)content withCallbackBlock:(TSFileManagerCallbackBlock)block;
- (void)filterFilesWithName:(NSString*)filter withCallbackBlock:(TSFileManagerCallbackBlock)block;
- (void)deleteFileWithName:(NSString*)name withCallbackBlock:(TSFileManagerCallbackBlock)block;
- (void)countFilesWithName:(NSString*)filter withCallbackBlock:(TSFileManagerCallbackBlock)block;
- (void)readFileWithName:(NSString*)name withCallbackBlock:(TSFileManagerCallbackBlock)block;

@end
