//
//  NSString+JavaParam.h
//  TestTask
//
//  Created by Hrybenuik Mykola on 9/25/15.
//  Copyright © 2015 Hrybenuik Mykola. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JavaParam)

- (NSString*)javaFormatParam;

@end
