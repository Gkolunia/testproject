//
//  TSFileManager.m
//  TestTask
//
//  Created by Hrybenuik Mykola on 9/25/15.
//  Copyright © 2015 Hrybenuik Mykola. All rights reserved.
//

#import "TSFileManager.h"
#import "NSString+JavaParam.h"

@implementation TSFileManager

- (void)saveFileWithName:(NSString *)name andContent:(NSString *)content withCallbackBlock:(TSFileManagerCallbackBlock)block {

    NSError *error;
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt", name]];
    [content writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (block) {
        if (!error)
            block(@[name.javaFormatParam], @"success");
        else
            block(@[name.javaFormatParam], error.localizedDescription);
    }
    
    
}

- (void)readFileWithName:(NSString *)name withCallbackBlock:(TSFileManagerCallbackBlock)block {
    NSError *error;
    
    NSString *nameWithOutExt = [name stringByDeletingPathExtension];
    
    NSString *contentOfFile = [NSString stringWithContentsOfFile:[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
                                                                  stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt", nameWithOutExt]]
                                                        encoding:NSUTF8StringEncoding
                                                           error:&error];
    if (block) {
        if (!error)
            block(@[name.javaFormatParam, contentOfFile.javaFormatParam], @"success");
        else
            block(@[name.javaFormatParam], error.localizedDescription);
    }
}

- (void)deleteFileWithName:(NSString *)name withCallbackBlock:(TSFileManagerCallbackBlock)block {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:name];
    NSError *error;
    [fileManager removeItemAtPath:filePath error:&error];
    if (block) {
        if (!error)
            block(@[name.javaFormatParam], @"success");
        else
            block(@[name.javaFormatParam], error.localizedDescription);
    }

}

- (void)filterFilesWithName:(NSString *)filter withCallbackBlock:(TSFileManagerCallbackBlock)block {

    NSError *error;
    
    NSArray *matches = [self p_findFilesByFilter:filter error:error];
    
    if (block) {
        if (error)
            block(@[filter.javaFormatParam, @"".javaFormatParam], error.localizedDescription);
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:matches options:0 error:nil];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    if (block) {
        if (!error)
            block(@[filter.javaFormatParam, jsonString.javaFormatParam], @"success");
        else
            block(@[filter.javaFormatParam, jsonString.length ? jsonString.javaFormatParam : @"".javaFormatParam], error.localizedDescription);
    }
    
}

- (void)countFilesWithName:(NSString *)filter withCallbackBlock:(TSFileManagerCallbackBlock)block {
    
    NSError *error;
    
    NSArray *matches = [self p_findFilesByFilter:filter error:error];
    
    if (block) {
        if (!error)
            block(@[filter.javaFormatParam, @(matches.count).stringValue.javaFormatParam], @"success");
        else
            block(@[filter.javaFormatParam, @(0).stringValue.javaFormatParam], error.localizedDescription);
    }
}

- (NSArray*)p_findFilesByFilter:(NSString *)filter error:(NSError *)error {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *arrayOfFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    if (error) {
        return nil;
    }
    
    NSString *placeholder =  @"%@";
    NSString *pattern = [NSString stringWithFormat:placeholder, filter];
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    id matches = nil;
    
    if (filter.length) {
        matches = [[NSMutableArray alloc] init];
        [arrayOfFiles enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([[regex matchesInString:obj options:NSMatchingReportProgress range:NSMakeRange(0, obj.length)] count]) {
                [matches addObject:obj];
            }
        }];
    }
    else {
        matches = arrayOfFiles;
    }
    
    
    return matches;
    
}

@end
