//
//  FileSystem.m
//  MyTestProject
//
//  Created by Hrybenuik Mykola on 9/25/15.
//  Copyright © 2015 Hrybenuik Mykola. All rights reserved.
//

#import "FileSystem.h"
#import "TSFileManager.h"

@implementation FileSystem {
    TSFileManager *_fileManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _fileManager = [TSFileManager new];
    }
    return self;
}

- (void)store:(NSString *)fileName :(NSString *)fileContent :(NSString *)callbackName {
    __weak typeof(self) weakSelf = self;
    [_fileManager saveFileWithName:fileName andContent:fileContent withCallbackBlock:^(NSArray *params, NSString *status) {
        [weakSelf p_createJavaScriptWithMethodName:callbackName params:params andStatus:status];
    }];
}

- (void)delete:(NSString *)fileName :(NSString *)callbackName {
    __weak typeof(self) weakSelf = self;
    [_fileManager deleteFileWithName:fileName withCallbackBlock:^(NSArray *params, NSString *status) {
        [weakSelf p_createJavaScriptWithMethodName:callbackName params:params andStatus:status];
    }];
}

- (void)list:(NSString *)filter :(NSString *)callbackName {
    __weak typeof(self) weakSelf = self;
    [_fileManager filterFilesWithName:filter withCallbackBlock:^(NSArray *params, NSString *status) {
        [weakSelf p_createJavaScriptWithMethodName:callbackName params:params andStatus:status];
    }];
}

- (void)read:(NSString *)fileName :(NSString *)callbackName {
    __weak typeof(self) weakSelf = self;
    [_fileManager readFileWithName:fileName withCallbackBlock:^(NSArray *params, NSString *status) {
        [weakSelf p_createJavaScriptWithMethodName:callbackName params:params andStatus:status];
    }];
}

- (void)count:(NSString *)filter :(NSString *)callbackName {
    __weak typeof(self) weakSelf = self;
    [_fileManager countFilesWithName:filter withCallbackBlock:^(NSArray *params, NSString *status) {
        [weakSelf p_createJavaScriptWithMethodName:callbackName params:params andStatus:status];
    }];
    
}

- (void)p_createJavaScriptWithMethodName:(NSString*)callBackName params:(NSArray*)params andStatus:(NSString*)status {
    
    
    NSString *paramsString = params.count ? [NSString stringWithFormat:@"%@, '%@'", [params componentsJoinedByString:@", "], status] : status;
    
    NSString *scriptsString = [NSString stringWithFormat:@"%@(%@)", callBackName, paramsString];
    
    if (_callbackScript) {
        _callbackScript(scriptsString);
    }
    
    
}


@end
