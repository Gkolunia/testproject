//
//  FileSystem.h
//  MyTestProject
//
//  Created by Hrybenuik Mykola on 9/25/15.
//  Copyright © 2015 Hrybenuik Mykola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JavaScriptCore/JavaScriptCore.h>


typedef void(^ExecuteJavaScriptBlock)(NSString *script);

@protocol FileSystemJS <JSExport>

- (void)store:(NSString*)fileName :(NSString*)fileContent :(NSString*)callbackName;
- (void)read:(NSString*)fileName :(NSString*)callbackName;
- (void)delete:(NSString*)fileName :(NSString*)callbackName;
- (void)list:(NSString*)filter :(NSString*)callbackName;
- (void)count:(NSString*)filter :(NSString*)callbackName;

@end

@interface FileSystem : NSObject <FileSystemJS>

@property (readwrite, copy) ExecuteJavaScriptBlock callbackScript;

- (void)setCallbackScript:(ExecuteJavaScriptBlock)callbackScript;

@end
